<?php

namespace Tests\GoldCalculator;

use GoldCalculator\Calculator;
use GoldCalculator\NbpApi;
use DateTime;

class CalculatorTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @param string $expectedClass
     * @param object $instance
     */
    public function assertInstance($expectedClass, $instance)
    {
        $assertMessage = $expectedClass . ' instance is expected, given: ';
        $assertMessage .= is_object($instance) ? get_class($instance) : gettype($instance);

        self::assertInstanceOf($expectedClass, $instance, $assertMessage);
    }

    public function mockUpData()
    {
        return [
            '2016-01-01' => 133.33,
            '2016-01-02' => 134.33,
            '2016-01-03' => 135.33,
            '2016-01-04' => 136.33,
        ];
    }

    /**
     * @return \PHPUnit_Framework_MockObject_MockBuilder
     */
    public function getApiMockup()
    {
        $mock = $this->getMockBuilder(NbpApi::class)
            ->disableOriginalConstructor()
            ->getMock();

        return $mock;
    }

    /**
     * @test
     */
    public function shouldConstructNewApiObject()
    {
        $calc = new Calculator($this->getApiMockup());
        self::assertInstance(Calculator::class, $calc);
    }

    /**
     * @test
     */
    public function shouldLoadData()
    {
        $mock = $this->getApiMockup();
        $mock->expects($this->once())
            ->method('getGoldPrices')
            ->will($this->returnValue($this->mockUpData()));

        $calc = new Calculator($mock);
        $calc->loadData(
            DateTime::createFromFormat('Y-m-d', '2016-01-01'),
            DateTime::createFromFormat('Y-m-d', '2016-01-30')
        );

        self::assertEquals($this->mockUpData(), $calc->getData());
    }

    /**
     * @test
     */
    public function shouldCalculate()
    {

        $mock = $this->getApiMockup();
        $mock->expects($this->once())
            ->method('getGoldPrices')
            ->will($this->returnValue($this->mockUpData()));

        $calc = new Calculator($mock);
        $calc->loadData(
            DateTime::createFromFormat('Y-m-d', '2016-01-01'),
            DateTime::createFromFormat('Y-m-d', '2016-01-30')
        );
        $data = $calc->invest(3000);
        self::assertEquals(true, is_string($data));
        self::assertEquals('If you would invest 3000 on 2016-01-01 you would have 3067,50 on 2016-01-04.' ."\n", $data);
    }
}