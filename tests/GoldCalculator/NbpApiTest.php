<?php

namespace Tests\GoldCalculator;

use GoldCalculator\ApiException;
use GoldCalculator\NbpApi;
use PHPUnit_Framework_TestCase;
use DateTime;

class NbpApiTest extends PHPUnit_Framework_TestCase
{

    /**
     * @param string $expectedClass
     * @param object $instance
     */
    public function assertInstance($expectedClass, $instance)
    {
        $assertMessage = $expectedClass . ' instance is expected, given: ';
        $assertMessage .= is_object($instance) ? get_class($instance) : gettype($instance);

        self::assertInstanceOf($expectedClass, $instance, $assertMessage);
    }

    /**
     * @test
     */
    public function shouldConstructNewApiObject()
    {
        $api = new NbpApi();
        self::assertInstance(NbpApi::class, $api);

    }

    /**
     * @test
     */
    public function shouldGetDataFromApi()
    {
        $api = new NbpApi();
        $data = $api->getGoldPrices(
            DateTime::createFromFormat('Y-m-d', '2016-01-01'),
            DateTime::createFromFormat('Y-m-d', '2016-01-30')
        );

        self::assertEquals(true, is_array($data));
    }

    /**
     * @test
     */
    public function shouldThrowException()
    {
        $api = new NbpApi();
        self::setExpectedException(ApiException::class);
        $data = $api->getGoldPrices(
            DateTime::createFromFormat('Y-m-d', '2010-01-01'),
            DateTime::createFromFormat('Y-m-d', '2010-01-30')
        );
    }
}