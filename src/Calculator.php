<?php

namespace GoldCalculator;

use DateTime;
use DateInterval;

/**
 * Class Calculator. "Calculates" string to return on given data
 *
 * @package GoldCalculator
 */
class Calculator
{
    const API_DAY_LIMIT = 90;

    /** @var \GoldCalculator\NbpApi */
    private $api;

    /** @var array */
    private $data = [];

    /**
     * Calculator constructor.
     *
     * @param NbpApi $api
     */
    public function __construct(NbpApi $api)
    {
        $this->api = $api;
    }

    /**
     * @return array
     */
    public function getData() : array
    {
        return $this->data;
    }

    /**
     * @param int $amount
     *
     * @return string
     */
    public function invest(int $amount) : string
    {
        $flipped = $this->floatFlip($this->data);
        $min = min($this->data);
        $max = max($this->data);

        $investOn = $flipped[$min];
        $payoutOn = $flipped[$max];

        $result = $this->calculate($min, $max, $amount);
        $dateInvest = DateTime::createFromFormat('Y-m-d', $investOn);
        $datePayout = DateTime::createFromFormat('Y-m-d', $payoutOn);
        $but = '';
        if ($dateInvest > $datePayout) {
            $but .= ' But that is not possible, sorry.' . "\n";
        }

        return 'If you would invest ' . $amount . ' on ' . $investOn . ' you would have ' . number_format($result, 2, ',', '') . ' on ' . $payoutOn  . '.' . "\n" . $but;
    }

    /**
     * @param DateTime $from
     * @param DateTime $to
     *
     * @return array
     */
    public function loadData(DateTime $from, DateTime $to)
    {
        $interval = $to->diff($from);
        try {
            if ($interval->days > self::API_DAY_LIMIT) {
                $this->data = $this->loadPartialData($from, $to);
            } else {
                $this->data = $this->api->getGoldPrices($from, $to);
            }
        } catch (ApiException $e) {
            echo $e->getMessage();die();
        }
    }

    /**
     * @param float $priceBuy
     * @param float $priceSell
     * @param int $amount
     *
     * @return float
     */
    private function calculate(float $priceBuy, float $priceSell, int $amount)
    {
        return ($amount / $priceBuy) * $priceSell;
    }

    /**
     * @param DateTime $from
     * @param DateTime $to
     *
     * @return array
     */
    private function loadPartialData(DateTime $from, DateTime $to)
    {
        $prices = [];
        $copyTo = clone $from;
        while ($copyTo < $to) {
            $copyFrom = clone $copyTo;
            $copyTo->add(new DateInterval('P90D'));
            if ($copyTo > $to) {
                $copyTo = $to;
            }
            try {
                $prices = $prices + $this->api->getGoldPrices($copyFrom, $copyTo);
            } catch (ApiException $e) {
                echo $e->getMessage();
            }
        }

        return $prices;
    }

    /**
     * @param array $array
     *
     * @return array
     */
    private function floatFlip(array $array) : array
    {
        $result = [];

        foreach ($array as $key => $item)
        {
            $result[$item] = $key;
        }

        return $result;
    }
}
