<?php

namespace GoldCalculator;
use DateTime;

/**
 * Class NbpApi
 *
 * @package GoldCalculator
 */
class NbpApi
{
    /** @var string */
    const BASE = 'http://api.nbp.pl/api/';

    /**
     * Gets gold prices on given date peroid
     *
     * @param \DateTime $from
     * @param \DateTime $to
     *
     * @return array
     * @throws ApiException
     */
    public function getGoldPrices(DateTime $from, DateTime $to)
    {
        try {
            $data = $this->makeCall(self::BASE . 'cenyzlota/' . $from->format('Y-m-d') . '/' . $to->format('Y-m-d'));
        } catch (ApiException $e) {
            throw new ApiException('No data on peroid ' . $from->format('Y-m-d') . ' to ' . $to->format('Y-m-d') . "\n");
        }

        return $data;
    }

    /**
     * Makes curl call
     *
     * @param $endpoint
     *
     * @return array
     * @throws ApiException
     */
    private function makeCall($endpoint) : array
    {
        $curlHandler = curl_init($endpoint);
        curl_setopt($curlHandler, CURLOPT_HTTPHEADER, [
            'Accept: application/json'
        ]);
        curl_setopt($curlHandler, CURLOPT_RETURNTRANSFER, 1);

        $response = curl_exec($curlHandler);

        if (!is_array(json_decode($response)))
            throw new ApiException('No data');

        return $this->convertResponse(json_decode($response));
    }

    /**
     * Prepares response from json object to simple array
     *
     * @param $responseJson
     *
     * @return array
     */
    private function convertResponse($responseJson) : array
    {
        $array = [];

        foreach ($responseJson as $item) {
            $array[$item->data] = $item->cena;
        }

        return $array;
    }
}
