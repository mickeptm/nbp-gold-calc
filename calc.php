<?php

require_once('./vendor/autoload.php');

if ($argc < 4) {
    echo 'Usage: calc dateFrom(Y-m-d) dateTo(Y-m-d) amount(int)'; die();
}

$api = new \GoldCalculator\NbpApi();
$calculator = new \GoldCalculator\Calculator($api);

$calculator->loadData(DateTime::createFromFormat('Y-m-d', $argv[1]), DateTime::createFromFormat('Y-m-d', $argv[2]));
echo $calculator->invest((int) $argv[3]);
